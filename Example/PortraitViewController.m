//
//  PortraitViewController.m
//  Example
//
//  Created by Pshin on 2015/5/3.
//  Copyright (c) 2015年 ganger. All rights reserved.
//

#import "PortraitViewController.h"

@interface PortraitViewController ()

@end

@implementation PortraitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self InitOnceParameter];
    [self InitOnceInterface];
    [self InitOnceTouch];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self InitParameter];
    [self InitInterface];
    [self InitTouch];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}


#pragma mark 初始化
//只會改變一次的參數
- (void)InitOnceParameter
{
    widthRatio = self.view.frame.size.width/320.0f;//多解析長的放大比例
    heightRatio = self.view.frame.size.height/480.0f;

}
//只會改變一次的界面
- (void)InitOnceInterface
{
    //讓不要變型（因為長寬放大比例不一樣）的View做多解析
    //-CenterView
    [self keepView:self.centerView withRatio:widthRatio];
    
    //-calBtn
    [self keepView:self.calBtn withRatio:widthRatio];
    
    //-showView
    [self keepView:self.showView withRatio:widthRatio];

    
    //-meausreBtn
    [self keepView:self.measureBtn withRatio:widthRatio];
    self.measureBtn.center = CGPointMake(self.measureBtn.center.x, self.measureBtn.center.y-self.measureBtn.frame.size.height*(widthRatio-1));
    
    //-infoBtn
    [self keepView:self.infoBtn withRatio:widthRatio];
    
    //-historyBtn
    [self keepView:self.historyBtn withRatio:widthRatio];

    
    
}
- (void)keepView:(UIView *)view withRatio:(float)ratio
{
    CGPoint centerPoint = view.center;
    view.frame = CGRectMake(0, 0, view.frame.size.width,  view.frame.size.height*widthRatio);
    view.center = centerPoint;
    

}
//只會改變一次的點擊事件
- (void)InitOnceTouch
{
    
    
}
//參數
- (void)InitParameter
{
    
    
    
}
//界面
- (void)InitInterface
{
    
    
    
}
//點擊事件
- (void)InitTouch
{
    
    
}

#pragma mark 界面更新
#pragma mark 邏輯判斷
#pragma mark 動畫
#pragma mark 監聽

#pragma mark 跳頁
#pragma mark 頁面轉向
//隱藏狀態列
- (BOOL)prefersStatusBarHidden
{
    return YES;
    
}
//橫向 直向
- (NSUInteger)supportedInterfaceOrientations
{
        return UIInterfaceOrientationMaskPortrait;//not horizontal
//    return UIInterfaceOrientationMaskLandscape;
    
}
#pragma mark 當記憶體不足時，在此釋放資源
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    if ([self isViewLoaded] && [self.view window] == nil)//if using surface or not
    {
        
    }
}

@end
