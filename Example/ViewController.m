//
//  ViewController.m
//  Example
//
//  Created by Pshin on 2015/4/25.
//  Copyright (c) 2015年 ganger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//只會進來一次
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self InitOnceParameter];
    [self InitOnceInterface];
    [self InitOnceTouch];
}

//看到畫面前
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self InitParameter];
    [self InitInterface];
    [self InitTouch];
 
}
//看到畫面後
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}
#pragma mark 初始化
//只會改變一次的參數
- (void)InitOnceParameter
{
    heightRatio = self.view.frame.size.height/320.0f;//多解析長的放大比例
    
    NSLog(@"hei:%f", self.view.frame.size.height);
    
}
//只會改變一次的界面
- (void)InitOnceInterface
{
    NSLog(@"1111%f;%f:%f;%f",self.exitBtn.frame.origin.x,self.exitBtn.frame.origin.y,self.exitBtn.frame.size.width,self.exitBtn.frame.size.height);
    [IdeabusLibrary geometryAllView:self.allView Orientation:1];
    self.allView.frame = CGRectMake(0, 0, self.allView.frame.size.width*heightRatio,  self.allView.frame.size.height);
    self.allView.center = self.view.center;
    
    NSLog(@"%f;%f:%f;%f",self.exitBtn.frame.origin.x,self.exitBtn.frame.origin.y,self.exitBtn.frame.size.width,self.exitBtn.frame.size.height);
    
}
//只會改變一次的點擊事件
- (void)InitOnceTouch
{
    
    
}
//參數
- (void)InitParameter
{
    
    
    
}
//界面
- (void)InitInterface
{
    

    
}
//點擊事件
- (void)InitTouch
{
    
    
}

#pragma mark 界面更新
#pragma mark 邏輯判斷
#pragma mark 動畫
#pragma mark 監聽

#pragma mark 跳頁
#pragma mark 頁面轉向
//隱藏狀態列
- (BOOL)prefersStatusBarHidden
{
    return YES;
    
}
//橫向 直向
- (NSUInteger)supportedInterfaceOrientations
{
//    return UIInterfaceOrientationMaskPortrait;//not horizontal
    return UIInterfaceOrientationMaskLandscape;
    
}
#pragma mark 當記憶體不足時，在此釋放資源
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    if ([self isViewLoaded] && [self.view window] == nil)//if using surface or not
    {
        
    }
}

@end
