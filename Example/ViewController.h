//
//  ViewController.h
//  Example
//
//  Created by Pshin on 2015/4/25.
//  Copyright (c) 2015年 ganger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdeabusLibrary.h"
@interface ViewController : UIViewController
{
    float heightRatio;

}
@property(weak,nonatomic)IBOutlet UIView *allView;//多解析
@property(nonatomic,weak)IBOutlet UIButton *exitBtn;//跳頁按鈕

- (IBAction)exitBtnTD:(id)sender;//按鈕名稱＋點擊事件名稱（TouchDown=TD）
@end

