//
//  AppDelegate.h
//  Example
//
//  Created by Pshin on 2015/4/25.
//  Copyright (c) 2015年 ganger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

