//
//  IdeabusLibrary.h
//  TMS
//
//  Created by Kimi on 2014/9/27.
//  Copyright (c) 2014年 Kimi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//網路應用添加
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface IdeabusLibrary : NSObject



+ (NSString *)gatVersion;
+ (UILabel *)setLabelUnderline:(UILabel *)label height:(int)height;
+ (void)animTextFieldDidEditing:(int)hoist view:(UIView *)view;
+ (void)animRemindView:(BOOL)isShowRemind remindView:(UIView *)view;
+ (UITextField *)showTextFieldleftView:(UITextField *)textField;
+ (void)Animation3DTransformAngle:(float)angle Orientation:(int)orientation View:(UIView *)view;
+ (void)setAnchorPoint:(CGPoint)anchorpoint forView:(UIView *)view;
+ (BOOL)isNetwork;
+ (NSString *)getDocumentPath;
+ (NSString *)getNowTime:(NSString *)format;
+ (BOOL)createNewFolder:(NSString *)documentPath folderName:(NSString *)folderName;
+ (BOOL)WriteToFile:(NSString *)filePath fileName:(NSString *)fileName fileData:(NSData *)fileData;
+ (BOOL)deleteFolder:(NSString *)filePath;
+ (NSData *)readToFile:(NSString *)filePath;
+ (void)geometryAllView:(UIView *)allView Orientation:(int)orientation;

+ (void)setLayerBorder:(id)sender radius:(float)radius;
    
@end
