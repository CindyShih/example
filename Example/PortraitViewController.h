//
//  PortraitViewController.h
//  Example
//
//  Created by Pshin on 2015/5/3.
//  Copyright (c) 2015年 ganger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortraitViewController : UIViewController
{
    float widthRatio;
    float heightRatio;

}
//多解析
@property(weak,nonatomic)IBOutlet UIView *centerView;
@property(weak,nonatomic)IBOutlet UIView *showView;
@property(weak,nonatomic)IBOutlet UIButton *calBtn;
@property(weak,nonatomic)IBOutlet UIButton *measureBtn;
@property(weak,nonatomic)IBOutlet UIButton *infoBtn;
@property(weak,nonatomic)IBOutlet UIButton *historyBtn;

@end
