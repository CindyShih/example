//
//  IdeabusLibrary.m
//  TMS
//
//  Created by Kimi on 2014/9/27.
//  Copyright (c) 2014年 Kimi. All rights reserved.
//

#import "IdeabusLibrary.h"

@implementation IdeabusLibrary



#pragma mark - 取得Library版本
+ (NSString *)gatVersion
{
    //最後編輯人員:Kimi
    return @"2014.10.27 22:40";
}


#pragma mark - 界面:在UILabel下畫底線 (label:物件 height:底線高度)
+ (UILabel *)setLabelUnderline:(UILabel *)label height:(int)height{
    
    CGRect textRect = [label.text boundingRectWithSize:label.frame.size
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:label.font}
                                         context:nil];
    
    UIView *viewUnderline=[[UIView alloc] init];
    CGFloat xOrigin=0;
    switch (label.textAlignment)
    {
        case NSTextAlignmentCenter:
            xOrigin=(label.frame.size.width - textRect.size.width)/2;
            break;
        case NSTextAlignmentLeft:
            xOrigin=0;
            break;
        case NSTextAlignmentRight:
            xOrigin=label.frame.size.width - textRect.size.width;
            break;
        default:
            break;
    }
    viewUnderline.frame=CGRectMake(xOrigin,
                                   textRect.size.height + height, //這邊可以調整底線的高低
                                   textRect.size.width,
                                   1);
    viewUnderline.backgroundColor=label.textColor;
    [label addSubview:viewUnderline];
    
    return label;
    
}

#pragma mark - 界面:設定圓角
+ (void)setLayerBorder:(id)sender radius:(float)radius{
    
    [[sender layer] setBorderWidth:0]; //設定邊框粗細
    [[sender layer] setBorderColor:[UIColor whiteColor].CGColor]; //邊框顏色
    [[sender layer] setMasksToBounds:YES]; //將超出邊框的部份做遮罩
    [[sender layer] setCornerRadius:radius]; //設定圓角程度
    
    //設定被景顏色
    //    [[alertSetView layer] setBackgroundColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.6].CGColor];
    
}

#pragma mark - 界面:伸縮鍵盤
+ (void)animTextFieldDidEditing:(int)hoist view:(UIView *)view
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    
    //設定動畫開始時的狀態為目前畫面上的樣子
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    view.frame = CGRectMake(view.frame.origin.x,
                                 hoist,
                                 view.frame.size.width,
                                 view.frame.size.height);
    
    [UIView commitAnimations];
}

#pragma mark - 界面:警告視窗的背景動畫
+ (void)animRemindView:(BOOL)isShowRemind remindView:(UIView *)view {
    if ( isShowRemind == true )
        view.alpha = 0;
    else
        view.alpha = 1;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    
    if ( isShowRemind == true )
        view.alpha = 1;
    else
        view.alpha = 0;
    
    [UIView commitAnimations];
    
}

#pragma mark - 界面:UITextField左邊留空白
+ (UITextField *)showTextFieldleftView:(UITextField *)textField{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    textField.leftView = view;
    textField.leftViewMode = UITextFieldViewModeAlways;
    view = nil;
    return textField;
}

#pragma mark - 界面:3D旋轉：角度 ：方向
+ (void)Animation3DTransformAngle:(float)angle Orientation:(int)orientation View:(UIView *)view
{
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = -1.0 / 500;
    transform = CATransform3DRotate(transform, angle * M_PI / 180.0f, orientation, 0, 0);
    
    view.layer.transform = transform;
    
}

#pragma mark - 界面:設定旋轉軸心
+ (void)setAnchorPoint:(CGPoint)anchorpoint forView:(UIView *)view{
    CGRect oldFrame = view.frame;
    view.layer.anchorPoint = anchorpoint;
    view.frame = oldFrame;
}

#pragma mark - 界面:多解析
+ (void)geometryAllView:(UIView *)allView Orientation:(int)orientation{
    
    if(orientation == 0){//直
        float ratio = allView.frame.size.width/320.0f;
        CGPoint point = allView.center;
        allView.frame = CGRectMake(0, 0, allView.frame.size.width, allView.frame.size.height*ratio);
        allView.center = point;
    }else{//橫
        float ratio = allView.frame.size.height/320.0f;
        CGPoint point = allView.center;
        allView.frame = CGRectMake(0, 0, allView.frame.size.width*ratio, allView.frame.size.height);
        allView.center = point;
    }
    
    
}

#pragma mark - 邏輯:判斷有沒有網路
+ (BOOL)isNetwork{
    //Create zero addy
    
    struct sockaddr_in Addr;
    bzero(&Addr, sizeof(Addr));
    Addr.sin_len = sizeof(Addr);
    Addr.sin_family = AF_INET;
    
    //結果存至旗標中
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *) &Addr);
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityGetFlags(target, &flags);
    
    if (flags & kSCNetworkFlagsReachable || flags & kSCNetworkReachabilityFlagsIsWWAN )
        return YES;
    else
        return NO;
    
}

#pragma mark - 邏輯:取得目前時間(格式)
+ (NSString *)getNowTime:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] ];
    NSDate *date = [NSDate date];
    
    //正規化的格式設定
    [formatter setDateFormat:format];
    
    //正規化取得的系統時間並顯示
    return [formatter stringFromDate:date];
}

#pragma mark - 邏輯:取得資料夾路徑(資料夾路徑)
+ (NSString *)getDocumentPath{
    //取得目錄
    NSArray *docDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [docDirectory objectAtIndex:0];
    
    return documentPath;
}

#pragma mark - 邏輯:建立新資料夾(資料夾路徑)
+ (BOOL)createNewFolder:(NSString *)documentPath folderName:(NSString *)folderName{
    
    //製作新資料夾的路徑
    NSString *newFolderPath = [documentPath stringByAppendingPathComponent:folderName];
    //建立新資料夾
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager createDirectoryAtPath:newFolderPath withIntermediateDirectories:YES attributes:nil error:nil])
        return true;
    else
        return false;
}

#pragma mark - 邏輯:寫入檔案(路徑)
+ (BOOL)WriteToFile:(NSString *)filePath fileName:(NSString *)fileName fileData:(NSData *)fileData{
    //製作新檔案的路徑
    filePath = [filePath stringByAppendingPathComponent:fileName];
    
    //建立空白新檔案
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager createFileAtPath:filePath contents:nil attributes:nil])
    {
        if ( [fileData writeToFile:filePath atomically:YES] )
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SavePictureDataEnd" object:@""];
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

#pragma mark - 邏輯:刪除資料夾
+ (BOOL)deleteFolder:(NSString *)filePath{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //判斷plist檔案存在時才刪除
    if ([fileManager fileExistsAtPath: filePath]) {
        [fileManager removeItemAtPath:filePath error:NULL];
        return true;
    }
    else
        return false;
   
}

#pragma mark - 邏輯:讀取檔案
+ (NSData *)readToFile:(NSString *)filePath{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //判斷plist檔案存在才讀取
    if ([fileManager fileExistsAtPath: filePath])
        return [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:filePath]];
    else
        return nil;
}





//UIEdgeInsets insets = UIEdgeInsetsMake(80, 55, 80, 55);
//// 伸缩后重新赋值
//scan_BGImg.image = [scan_BGImg.image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeTile];



@end
